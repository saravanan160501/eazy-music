//
//  DataAccessLayer.swift
//  Music Deviser
//
//  Created by Mohan Saravanan on 19/6/20.
//  Copyright © 2020 Smartics. All rights reserved.
//

import Foundation
import MediaPlayer

class DataAccessLayer: ObservableObject {
    @Published var musicController = MPMusicPlayerController.systemMusicPlayer
}
