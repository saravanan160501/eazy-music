//
//  Extensions.swift
//  Music Deviser
//
//  Created by Mohan Saravanan on 19/6/20.
//  Copyright © 2020 Smartics. All rights reserved.
//

import SwiftUI
import Combine

let messagePublisher = PassthroughSubject<String, Never>()


extension View {
    func pinchToZoom() -> some View {
        self.modifier(PinchToZoom())
    }
}

extension UIWindow {
    open override func motionEnded(_ motion: UIEvent.EventSubtype, with event:   UIEvent?) {
        if motion == .motionShake {
            messagePublisher.send("ShakeGesture")
        }
    }
}
