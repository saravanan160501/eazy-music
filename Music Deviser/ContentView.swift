//
//  ContentView.swift
//  Music Deviser
//
//  Created by Mohan Saravanan on 19/6/20.
//  Copyright © 2020 Smartics. All rights reserved.
//

import SwiftUI
import MediaPlayer
import KeyboardObserving

struct ContentView: View {
    //MARK: Environment Objects
    @EnvironmentObject var DataAccessLayer: DataAccessLayer
    
    @State var zapMin = 0.50
    @State var minutes = 15.0
    @State var isRunning = false
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                Spacer()
                HStack {
                    Spacer()
                    Image(uiImage: DataAccessLayer.musicController.nowPlayingItem?.artwork?.image(at: CGSize(width: 3600, height: 3600)) ?? UIImage(systemName: "music.note")!)
                        .renderingMode(.original)
                        .resizable()
                        .pinchToZoom()
                        .scaledToFit()
                        .cornerRadius(20)
                        .padding()
                    Spacer()
                }
                VStack(alignment: .leading) {
                    Text(DataAccessLayer.musicController.nowPlayingItem?.title ?? "").font(.title).bold()
                    Text(DataAccessLayer.musicController.nowPlayingItem?.artist ?? "").font(.headline)
                    Text(DataAccessLayer.musicController.nowPlayingItem?.albumTitle ?? "").font(.subheadline)
                }.padding(.horizontal)
                Spacer()
                if !isRunning {
                    VStack(alignment: .leading) {
                        Text("Track Duration: ").font(.headline)
                        Picker(selection: $zapMin, label: EmptyView()) {
                            Text("15s").tag(0.25)
                            Text("30s").tag(0.50)
                            Text("45s").tag(0.75)
                            Text("60s").tag(1.00)
                            Text("75s").tag(1.25)
                            Text("90s").tag(1.50)
                        }.pickerStyle(SegmentedPickerStyle())
                            .padding(.bottom)
                        Text("How long do you want to play? (Minutes)").font(.headline)
                        TextField("Minutes", value: $minutes, formatter:  NumberFormatter())
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }.padding()
                }
                Spacer()
                HStack {
                    Spacer()
                    Button(action: {
                        if !self.isRunning {
                            self.isRunning = true
                            let runs = Int((self.minutes / self.zapMin))
                            for i in 0..<runs + 1 {
                                self.skip(i: Double(i))
                                if i == runs {
                                    DispatchQueue.main.asyncAfter(deadline: (.now() + (self.zapMin * 60 * Double(i)))) {
                                        self.isRunning = false
                                        self.DataAccessLayer.musicController.pause()
                                    }
                                }
                            }
                        } else {
                            self.isRunning = false
                            self.DataAccessLayer.musicController.pause()
                        }
                    }) {
                        Text(String(format: "%.0f", (minutes / zapMin) + (isRunning ? 1 : 0)))
                            .font(.title)
                            .bold()
                            .foregroundColor(.primary)
                            .frame(width: 80, height: 80)
                            .background(BlurView())
                            .clipShape(Circle())
                            .shadow(radius: 10)
                    }.padding(.bottom)
                    
                    Spacer()
                }.padding()
            }.keyboardObserving()
                .background(BlurView().edgesIgnoringSafeArea(.all))
                .background(Image(uiImage: DataAccessLayer.musicController.nowPlayingItem?.artwork?.image(at: CGSize(width: 3600, height: 3600)) ?? UIImage(systemName: "music.note")!).renderingMode(.original).resizable().scaledToFill().edgesIgnoringSafeArea(.all))
                .navigationBarTitle("Music Deviser", displayMode: .large)
        }.navigationViewStyle(StackNavigationViewStyle())
            .onReceive(messagePublisher) { (message) in
                if message == "changed" {
                    self.DataAccessLayer.musicController = MPMusicPlayerController.systemMusicPlayer
                } else if message == "ShakeGesture" {
                    self.DataAccessLayer.musicController.skipToNextItem()
                }
        }
    }
    func skip(i: Double) {
        DispatchQueue.main.asyncAfter(deadline: (.now() + (self.zapMin * 60 * i))) {
            if self.isRunning {
                self.DataAccessLayer.musicController.pause()
                self.DataAccessLayer.musicController.skipToNextItem()
                let totalDuration = self.DataAccessLayer.musicController.nowPlayingItem?.playbackDuration
                if let totalDuration = totalDuration {
                    let randomTimeHeader = Int.random(in: 0..<Int(totalDuration - (self.zapMin + 15)))
                    self.DataAccessLayer.musicController.currentPlaybackTime = TimeInterval(randomTimeHeader)
                    self.DataAccessLayer.musicController.play()
                    if self.minutes - self.zapMin < 0 {
                        self.minutes = 0
                    } else {
                        self.minutes -= self.zapMin
                    }
                }
            }
        }
    }
}
