//
//  BlurView.swift
//  Music Deviser
//
//  Created by Mohan Saravanan on 19/6/20.
//  Copyright © 2020 Smartics. All rights reserved.
//

import SwiftUI

struct BlurView: UIViewRepresentable {
    func makeUIView(context: UIViewRepresentableContext<BlurView>) -> UIView {
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .systemThinMaterial))
        blurView.backgroundColor = .clear
        return blurView
    }
    func updateUIView(_ uiView: UIView,
                      context: UIViewRepresentableContext<BlurView>) {}
}

